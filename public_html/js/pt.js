var baseURI = "http://130.211.182.69:8080/pt/";

var $content = $("#content")
var views = {};
var viewsLoaded = 0;
var refreshTimerID = -1;

var viewConfig = [
    {
        page: "select_stop",
        name: "selectStop"
    },
    {
        page: "location_display",
        name: "locationDisplay"
    },
    {
        page: "location_updating",
        name: "locationUpdating"
    },
    {
        page: "predictions",
        name: "predictions"
    }
];

// Load the underscore template views
for (i = 0; i < viewConfig.length; i++) {
    (function(count) {
        $.get("views/" + viewConfig[count].page + ".html", function(html) {
            views[viewConfig[count].name] = _.template(html);
            viewsLoaded++;
            tryLocation();
        });
    })(i);
}

// Include the location functions and request the browser location
$.getScript("js/location.js", function() {
    tryLocation();
});

/**
 * Requests that the browser location be updated, and displays the updating
 * status to the user.
 * @returns {undefined}
 */
function updateLocation() {
    window.requestLocation(locationUpdated);
    $("#location", $content).html(views.locationUpdating());    
}

/**
 * The callback that receives the browser's location.
 * @param {number} lat the location latitude
 * @param {number} lon the location longitude
 * @returns {undefined}
 */
function locationUpdated(lat, lon) {
    $("#location", $content).html(views.locationDisplay({lat: lat, lon: lon}))
            .find("#location-refresh").click(updateLocation);
    requestClosestStops(lat, lon);
}

/**
 * Checks if all prerequisite views and scripts have loaded, and if so, starts
 * the location request.
 * @returns {undefined}
 */
function tryLocation() {
    if ((viewsLoaded === viewConfig.length) && (_.isFunction(window.requestLocation))) {
        updateLocation();
    }
}

/**
 * Called when all views have been loaded.
 * @returns {undefined}
 */
function allViewsLoaded() {
    tryLocation();
}

/**
 * Requests the closest stops from the API server and renders the results.
 * @param {number} lat the latitude to request stops close to
 * @param {number} lon the longitude to request stops close to
 * @returns {undefined}
 */
function requestClosestStops(lat, lon) {
    var $selectStop = $content.find("#select-stop").empty();
    $.ajax(baseURI + "route/closest/" + lat + "," + lon)
            .done(function(data) {
                $selectStop.html(views.selectStop({stops: data}))
                        .find("select").change(refreshPredictions);
                refreshPredictions();
            });
}

/**
 * Called when the transit stop selection is updated by the user. Starts a
 * request for the predictions for the requested stop. Also starts a 20 second
 * timer to refresh again.
 * @returns {undefined}
 */
function refreshPredictions() {
    if (refreshTimerID !== -1) {
        clearTimeout(refreshTimerID);
        refreshTimerID = -1;
    }
    var $predictions = $content.find("#predictions").empty();
    $.ajax(baseURI + "predictions/" + $content.find("#select-stop select").val())
            .done(function(data) {
                $predictions.html(views.predictions({predictions: data}));
            })
            .always(function() {
                refreshTimerID = setTimeout(refreshPredictions, 20000);                
            });
}