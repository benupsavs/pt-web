// Browser location functions.

var locationCallback;

window.requestLocation = function(callback) {
    locationCallback = callback;
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(browserLocationUpdated);
    } else {
        // Location unavailable
        callback();
    }
};

window.browserLocationUpdated = function(position) {
    locationCallback(position.coords.latitude, position.coords.longitude);
};