# README #

Public Transport Departure Times web project.

### Purpose ###

* This is the client side component of the project.
* The [pt-api](https://bitbucket.org/benupsavs/pt-api) project is the server side.

### Setup ###

* Install the contents of public_html to your local computer, or a web server.
* Modify the top line of js/pt.js to point to the API server.
* Navigate to the root of the website in a web browser.

### Usage ###
* When the app is launched, the app will request the location from the browser.
* Once the app has the browser location, it will request the 5 closest transit stops from the server.
* Departure times are displayed for the closest stop in the list, and refreshed every 20 seconds.
* The stop may be changed using the select box.

### Architecture ###

* Bootstrap 3, jQuery, Underscore.js - mostly used for client-side templates.
* Code review
* Other guidelines